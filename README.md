# Spring Valley Frontend Developer Exam

## Instructions

- Fork this repo and then clone.
- Install dependencies by typing the command : `npm install`.<br>
  \*\* Note: Make sure you're able to see the `package.json` in the directory you're in by typing `ls` or `dir` in your terminal.
- Open the project with VS Code.
- Start coding.

## Overview

This is a basic template inspired by this [dribble](https://dribbble.com/shots/5932333-Aquaman-Redesign-Concept/attachments).

### `npm start`

Runs the project in the development mode. <br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm lint:fix`

Automatically fix problems in your `main.js` file.

### Expected Outcomes

1. Template - **intern**<br>

   - Applicant must be familiar enough with source control to be able to fork this repo, and send a pull request at the end of the process
   - Applicant must be able to modify `index.html` and make the navigation bar responsive from bigger (desktop) and smaller (mobile) screens using [bootstrap](https://getbootstrap.com/docs/4.3/getting-started/introduction/).
     \*\* Note: Make sure it works on Internet Explorer, Chrome and Firefox.

2. Template - **employment**<br>

   - Applicant must be familiar enough with source control to be able to fork this repo, and send a pull request at the end of the process
   - Applicant must be able to install dependencies in the `package.json` file and able to kickstart the development server.
   - Applicant must be able to make the navigation bar responsive from bigger (desktop) and smaller (mobile) screens.
     \*\* Note: Make sure it also works on Internet Explorer, Chrome and Firefox.
   - Applicant must be able to use developer tools to check if it works on mobile screens.
